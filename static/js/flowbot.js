$(document).ready(function(){

    // $('#text_area').on('input propertychange paste', function(event) {
    $('textarea').on('keyup', function(event) {
        // console.log('CALLED');

        // Push the input to Flask
        $.post('/validate', {
            body: $('textarea').val(),
        }).done(function(response) {
            // console.log('Got Response: ' + response);
            // Set background red on failure
            if (response !== "OK") {
                $("textarea").css("background-color","red");
            }
            else {
                $("textarea").css("background-color","white");
            }
        });
    });

    $(function() {
        $('.confirm').click(function(e) {
            e.preventDefault();
            if (window.confirm("Are you sure!?!?")) {
                location.href = this.href;
            }
        });
    });
});
