# Flowbot

## About

Flowbot is a work*flow* ro*bot*, meaning that it automates workflow tasks,
and is driven through issues that are part of JIRA workflows.

## Development

### Preparing Your Workstation

Development is conducted within a VirtualBox virtual machine (VM).  Therefore,
you must install the following on your host workstation:

1. [VirtualBox](https://www.virtualbox.org/)
1. [Vagrant](https://www.vagrantup.com/)
1. [Packer](https://www.packer.io/) (Optional)

>
> **Windows Users**
>
> The easiest way to install everything above is to use
> [Chocolatey](https://chocolatey.org/), which is a package manager for Windows
> (like apt-get for Windows).
>
> To install Chocolatey, find your Command Prompt icon (or find it on your
> Start menu), right-click on it, and choose *Run as administrator*.  Then run
> the following command:
>
> ```
> @powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
> ```
>
> From the same Command Prompt window, install the items above by running the
> following command:
>
> ```
> choco install -y virtualbox vagrant packer
> ```
>
> You should reboot your computer afterwards.
>

### Working with the Virtual Machine

After installing the above, start your development VM by running the following
command from a command prompt from the project directory (the directory
containing `Vagrantfile` and this `README.md` file), which may take a
several minutes upon first execution:

```
vagrant up
```

The username and password are both `vagrant` for logging into the VM, and this
user has `sudo` permission.

The project directory is mounted as a synched folder that appears in the
`/home/vagrant/flowbot` folder within the VM.  This is the directory within the
VM where you will conduct your development work.

You may also connect to the VM via SSH by running the following command from
a command prompt on your workstation (again from this project directory):

```
vagrant ssh
```

To gracefully shutdown the VM, running the following command from a command
prompt on your workstation:

```
vagrant halt
```

Finally, to rebuild your VM in case the base box is updated, run the
following commands:

```
vagrant halt
vagrant destroy
vagrant up
```

### Starting the Application Components

The Flowbot system consists of the following primary components:

1. The **Flask** web application that handles JIRA webhooks
1. The **Celery** worker that schedules tasks handed off from the Flask application
1. **Redis**, which is used by Celery for managing task scheduling

When your VM starts, **the redis server is automatically started**, so there is
nothing required for this component.  Only the Flask application and the Celery
worker must be started manually.

To **start the Celery worker**, open a Terminal window within the VM and
execute the following commands:

```
workon flowbot
celery worker -A main.celery
```

To **start the Flask application**, open another Terminal window within the VM
and execute the following commands:

```
workon flowbot
python main.py
```

Note that the `workon flowbot` command is made available by the Python
[virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/),
which is configured as part of the VM provisioning.  This command
automatically activates the `flowbot` virtual environment and changes
directory to the project directory.

### Developing with PyCharm

#### Initial Configuration

[PyCharm](https://www.jetbrains.com/pycharm/) is installed as part of the VM
provisioning, and there should be only a couple of configuration steps required
the first time you create your VM.

To start PyCharm, double-click on the PyCharm icon that should appear on your
VM's desktop.  The first time you start PyCharm, you will be presented with the
following options:

* Create New Project
* Open
* Check out from Version Control

Choose **Open**, which will present a dialog box with a directory tree and
the `/home/vagrant` directory initially selected.  Select the `flowbot`
sub-directory, so the full path of the desired directory is
`/home/vagrant/flowbot`, which is the synched folder.  After selecting the
`flowbot` sub-directory, click `OK`.

After a few moments, you should see some messages.  After clearing the
message boxes, you should see the `flowbot` project listed at the top of the
Project view along the left side of the window.

Now you must configure the `flowbot` project to use the Python interpreter
from the `flowbot` virtual environment.  This will enable code completion
based upon the version of Python and the packages installed in that virtual
environment.  To do so, take the following steps:

1. From the **File** menu, select **Settings...**.
1. Along the left of the **Settings** dialog, expand the top-level item
**Project: flowbot**, then select **Project Interpreter**.
1. At the top of the right-side portion of the dialog, click the drop-down
for the **Project Interpreter** field and select `/home/vagrant/
.virtualenvs/flowbot/bin/python` from the list.
1. After the list of packages and versions is populated, click `OK`.

You should now be ready to develop using PyCharm by expanding the `flowbot`
project folder on the left-hand side.

#### Plugins

There are numerous PyCharm plugins available.  Given that this project
contains `bash` scripts and markdown files, you may wish to install the
following plugins that make it nicer to edit such files:

* BashSupport
* Markdown support
* IdeaVim (if you like `vi` keybindings for editing all file types)

To install a plugin, do the following:

1. From the **File** menu, select **Settings...**.
1. Select the top-level **Plugins** item along the left.
1. Below the list of currently installed plugins, click the **Browse
repositories...** button.
1. If you know the name of the plugin, such as one of the plugins mentioned
above, start typing the name of the plugin and PyCharm will perform an
incremental search.
1. Select the desired plugin from the list, and click the **Install** button
at the top right.
1. Click `Close`, or clear the search box and look for another plugin.
1. After closing the repository browser, click `OK`.
1. When prompted, click `Restart` to restart PyCharm and enable the plugin(s).

### Coding Guidelines

In Python, [PEP8](https://www.python.org/dev/peps/pep-0008/) is the style
guide for Python code, and it is the style guide used for this project.

To make it easy to check code against the PEP8 style guide, the following are
configured within the VM:

* The `pep8` package is installed within the `flowbot` virtual environment.
* PyCharm's code style is configured to adhere to PEP8

To leverage the `pep8` package, from a Terminal window in the VM, you can
simply run the `pep8` command, giving it one or more Python files to check,
and it will print out a list of violations (or print nothing if there are no
violations).

For example:

```text
$ pep8 flow.py
flow.py:15:1: E302 expected 2 blank lines, found 1
flow.py:23:80: E501 line too long (88 > 79 characters)
flow.py:25:13: W291 trailing whitespace
flow.py:28:37: E128 continuation line under-indented for visual indent
flow.py:29:24: E124 closing bracket does not match visual indentation
flow.py:30:16: W291 trailing whitespace
flow.py:36:5: E303 too many blank lines (3)
flow.py:39:80: E501 line too long (88 > 79 characters)
flow.py:45:80: E501 line too long (86 > 79 characters)
flow.py:53:80: E501 line too long (88 > 79 characters)
flow.py:64:80: E501 line too long (91 > 79 characters)
flow.py:64:90: E202 whitespace before ')'
flow.py:65:22: E201 whitespace after '['
flow.py:65:66: E202 whitespace before ']'
...
flow.py:242:1: W391 blank line at end of file
```

However, since PyCharm's code style is configured to adhere to the PEP8
style, you can use PyCharm's built-in code reformatting capability.  To let
PyCharm correct violations such as those shown above, open the file in
question, then from the **Code** menu, select **Reformat Code**.  Note that
the standard key combination for this function (Ctrl-Alt-L) will not work
because it is bound by the VM to lock the screen.

### Using Git

At this point, a very old version of Git is installed out-of-the-box on the
VM, and PyCharm complains about it.  Therefore, until we decide to
place the latest version of Git on the VM, you should perform all of your Git
commands from your workstation.
