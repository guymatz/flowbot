from celery import Celery
from processes import Process
from celery import current_app
from celery.utils.log import get_task_logger
from celery.schedules import crontab
from jira import JIRA
from processes.confluence import Confluence
import re
import os, sys
import yaml
import requests
from config import config, Config

# This is necessary for celery to be able to see issuetypes, etc.
# Not sure why . . .
sys.path.append(os.getcwd())


env = os.environ['FLOWBOT_ENV']
if os.path.exists('.env'):
    #print('Importing environment from .env...')
    for line in open('.env'):
        k, v = line.strip().split('=')
        os.environ[k.strip()] = v.strip()

celery = Celery(__name__)
celery.config_from_object(config[env])
clogger = get_task_logger(__name__)
clogger.debug("Scheduled Jobs 2: %s" % current_app.conf['SCHEDULED_JOBS'])
clogger.debug("ENV : %s" % env)
clogger.debug("Config: %s" % celery.conf)

# Celerybeat
CELERYBEAT_JOBS = {}
for title,job in current_app.conf['SCHEDULED_JOBS'].items():
    #print("Scheduled Jobs 3: %s" % current_app.conf['SCHEDULED_JOBS'])
    clogger.debug("Scheduled Jobs 4: %s" % current_app.conf['SCHEDULED_JOBS'])
    CELERYBEAT_JOBS[title] = {
        'task': 'tasks.' + job['method'],
        'schedule': crontab(**job['schedule']),
    }
    if job.get('jql'):
        CELERYBEAT_JOBS[title]['args'] = (job['jql'],)
clogger.info("Celery Beat: %s" % CELERYBEAT_JOBS)
celery.conf['CELERYBEAT_SCHEDULE'] = CELERYBEAT_JOBS
#celery.conf['CELERY_IMPORTS'] = ("tasks")
clogger.debug("celery.conf = %s" % celery.conf)


# jira object
jira = JIRA(server=current_app.conf['JIRA_SERVER'],
            basic_auth=( current_app.conf['JIRA_USER'],
                         current_app.conf['JIRA_PASS'])
            )

@celery.task(name='tasks.close_tickets')
def close_tickets(jql):
    clogger.info("In close_tickets: %s" % jql)
    try:
        tix_to_close = jira.search_issues(jql)
        clogger.info("Num tix to close: %i" % len(tix_to_close))
    except Exception as e:
        clogger.warn("Error with jql %s: %s" % (jql, e.text))
        return
    for ticket in tix_to_close:
        issue = Process(jira, ticket, current_app)
        try:
            issue.transition_issue('Flowbot Auto Close Request', 'Your request is being closed due to inactivity')
            clogger.info("Ticket has been closed: %s" % (ticket))
        except Exception as e:
            clogger.warn("Could not close %s: %s" % (ticket, e))

@celery.task(name='tasks.deactivate_users')
def deactivate_users(jql):
    clogger.info("In deactivate_tickets: %s" % jql)
    try:
        tix_to_process = jira.search_issues(jql, fields="*all")
        clogger.info("Num tix to close: %i" % len(tix_to_process))
    except Exception as e:
        clogger.warn("Error with jql %s: %s" % (jql, e))
        return
    for ticket in tix_to_process:
        issue = get_processing_object(ticket)
        issue.process_description()

@celery.task(name='tasks.reset_exclusions')
def reset_exclusions(jql):
    clogger.info("In tasks.reset_exclusions: %s" % jql)
    try:
        tix_to_process = jira.search_issues(jql, fields="*all")
        clogger.info("Num tix to reset exclusions: %i" % len(tix_to_process))
    except Exception as e:
        clogger.warn("Error with jql %s: %s" % (jql, e))
        return
    for ticket in tix_to_process:
        issue = get_processing_object(ticket)
        issue.reset_exclusions()

@celery.task(name='tasks.cancel_sap_access')
def cancel_sap_access(jql):
    clogger.info("In tasks.cancel_sap_access: %s" % jql)
    print("In tasks.reset_exclusions: %s" % jql)
    try:
        tix_to_process = jira.search_issues(jql, fields="*all")
        clogger.info("Num tix to Cancel SAP Access: %i" % len(tix_to_process))
    except Exception as e:
        clogger.warn("Error with jql %s: %s" % (jql, e))
        return
    for ticket in tix_to_process:
        issue = get_processing_object(ticket)
        issue.cancel_sap_access()

@celery.task(name='tasks.expire_sap_access')
def expire_sap_access(jql):
    clogger.info("In tasks.expire_sap_access: %s" % jql)
    try:
        tix_to_process = jira.search_issues(jql, fields="*all")
        clogger.info("Num tix to Expire SAP Access: %i" % len(tix_to_process))
    except Exception as e:
        clogger.warn("Error with jql %s: %s" % (jql, e))
        return
    for ticket in tix_to_process:
        issue = get_processing_object(ticket)
        issue.expire_sap_access()

@celery.task(name='tasks.set_exclusions')
def set_exclusions(jql):
    clogger.info("In tasks.set_exclusions: %s" % jql)
    try:
        tix_to_process = jira.search_issues(jql, fields="*all")
        clogger.info("Num tix to set exclusions: %i" % len(tix_to_process))
    except Exception as e:
        clogger.warn("Error with jql %s: %s" % (jql, e))
        return
    for ticket in tix_to_process:
        issue = get_processing_object(ticket)
        issue.process_attachment()

@celery.task(name='tasks.update_toggles_pages')
def update_toggles_pages(p):
    clogger.warn("In tasks.update_toggles_pages: %s" % p)
    config_endpoint = 'r/api/configuration'
    old_toggles = None
    brands = {'factory': 'bwi-fscrnas-p01',
              'jcrew': 'bwi-fscrnas-p01',
              'madewell': 'bwi-fscrnas-p01'
              }
    for brand in brands:
        toggle_file = "%s_toggles.json" % brand
        current_toggles = requests.get("http://%s/%s" % (brands[brand],
                                                config_endpoint)).text
        try:
            with open(toggle_file) as file:
                old_toggles = file.read()
        except IOError as e:
            clogger.warn("No Toggles files exists to compare against: %s" % e)
            old_toggles = None
        #clogger.warn("old_toggles: %s" % (old_toggles))
        #clogger.warn("current_toggles: %s" % (current_toggles))
        if current_toggles != old_toggles:
            try:
                c = Confluence(
                           #url=current_app.conf['JIRA_SERVER']+'/wiki',
                           url='https://jcrewtracker.jira.com/wiki',
                           username=current_app.conf['JIRA_USER'],
                           password=current_app.conf['JIRA_PASS']
                       )
                resp = c.storePageContent('DR', '%s Toggles' % brand.title(),
                                         current_toggles)
                with open(toggle_file, 'w') as file:
                    file.write(current_toggles)
                clogger.warn("New toggles page has been set in WIKI: %s, %s" % (resp, resp.text))
            except IOError as e:
                clogger.warn("Could not write to %s: %s" % (toggle_file, e))
            except Exception as e:
                clogger.warn("Something went wrong: %s" % (e))

def get_issue_object(jira, issue, current_app):
    itype = issue.fields.issuetype.name.lower()
    itype = re.sub('[^A-z0-9]', ' ', itype)
    Itype = itype.title()
    itype = re.sub(' ', '_', itype)
    Itype = re.sub(' ', '', Itype)
    try:
        issue_mod = 'processes.issuetypes.%s' % itype
        clogger.debug("TRYING TO GET %s FROM %s" % (Itype, issue_mod))
        mod = __import__(issue_mod, fromlist=[Itype])
    except Exception as e:
        clogger.warn("Error: " + str(e))
        raise Exception(e)
    clogger.info("Returning an object of type %s/%s" % (issue_mod, Itype))
    return getattr(mod, Itype)(jira, issue, current_app.conf)

def get_workflow(issue):
    clogger.debug('Looking for workflow for %s' % (issue))
    itype = issue.fields.issuetype.name
    try:
        clogger.debug('Looking for workflow for %s/%s' % (issue, itype))
        project = issue.fields.project.key
        description = jira.project(project).description
        if len(description) == 0:
            clogger.warn('Empty description for Project: %s' % issue.fields.project.key)
            return
        workflows = yaml.load(description)['workflows']
        clogger.debug("Workflows available: %s" % workflows)
        workflow = workflows.get(itype, None)
        clogger.info('Returning  %s Workflow for %s' % (project.lower() + '.' + workflow, issue))
        return workflow
    except Exception as e:
        clogger.debug('Could not find Workflow for %s: %s' % (issue, e))
        return None

def get_processing_object(issue):
    #clogger.warn('Checking for Workflow for %s . . .' % (issue))
    workflow = get_workflow(issue)
    if workflow:
        try:
            Workflow = workflow.capitalize()
            Itype = Workflow
            proc_mod = 'processes.projects.%s.%s' % (issue.fields.project.key.lower(), workflow)
            #clogger.warn('Creating Workflow object for %s / %s' % (proc_mod, Itype))
            mod = __import__(proc_mod, fromlist=[Itype])
        except Exception as e:
            clogger.warn('ERROR Creating Workflow object for %s / %s: %s' % (proc_mod, Itype, e))
            raise Exception(e)
    else:
        itype = issue.fields.issuetype.name.lower()
        itype = re.sub('[^A-z0-9]', ' ', itype)
        Itype = itype.title()
        itype = re.sub(' ', '_', itype)
        Itype = re.sub(' ', '', Itype)
        try:
            proc_mod = 'processes.issuetypes.%s' % itype
            mod = __import__(proc_mod, fromlist=[Itype])
        except Exception as e:
            clogger.warn("Error: " + str(e))
            raise Exception(e)
    clogger.debug("Returning an object of type %s/%s(%s, %s, celery.conf)" % (mod, Itype, jira, issue))
    try:
        attr = getattr(mod, Itype)(jira, issue, celery.conf)
    except Exception as e:
        clogger.warn("Could not get attr for %s: %s" % (issue, e))
        raise Exception(e)
    return attr

def async_process(issue, method, args=None):
    '''
    Let celery process the issues with the method (and args)
    :param issue:
    :param method:
    :param args:
    :return: Nothing
    '''
    clogger.warn("In apos, going to run %s(%s) on %s" % (method, args, issue))
    try:
        issue_obj = get_processing_object(issue)
    except Exception as e:
        clogger.warn("Error getting processing object: " + str(e))
        return
    try:
        if args:
            getattr(issue_obj, method)(args)
        else:
            getattr(issue_obj, method)()
    except Exception as e:
        clogger.warn("In apos, when running %s(%s) - %s" % (method, args, e))

@celery.task()
def test_query_jira_via_jql(jql):
    return len(query_jira_via_jql(jql))

def query_jira_via_jql(jql, fields="*all,comment,changelog", expand="changelog,transitions.fields"):
    try:
        issues = jira.search_issues(jql, fields=fields, expand=expand)
        clogger.debug("Issues: %s" % (issues))
        return issues
    except Exception as e:
        clogger.warn("Error with jql '%s': %s" % (jql, e))
        raise e

@celery.task()
def query_jira(webhook_project=None):
    '''
    Queries Jira based on JQL in config, i.e. current_app.conf['JIRA_PROJECTS']
    Returns list of issues
    :param webhook_project:
    :return: list of jira issues
    '''
    # Process unprocessed tickets on startup
    clogger.debug("Project/Issue Types: %s" % current_app.conf['JIRA_PROJECTS'])
    projects = current_app.conf['JIRA_PROJECTS']
    if webhook_project:
        projects = { p: projects[p] for p in projects if p == webhook_project }
    clogger.debug("Projects: %s" % projects)
    for project in projects:
        for status in current_app.conf['JIRA_PROJECTS'][project]:
            method = status['method']
            jql = "%s ORDER BY updated DESC" % status['jql']
            args = status.get('args')
            clogger.debug("jql: %s" % jql)
            print("jql: %s" % jql)
            try:
                issues = query_jira_via_jql(jql)
                print("issues: %s" % issues)
            except Exception as e:
                clogger.error("Error with jql %s: %s" % (jql, e))
                continue
            clogger.debug("found %i issues from: %s" % (len(issues), jql))
            for issue in issues:
                clogger.info("Processing %s with method: %s(%s)" % (issue, method, args))
                data = {}
                data['issue'] = issue.raw
                async_process(issue, method, args)

if __name__ == "__main__":
    print("Run with somthing like 'celery worker -A tasks.celery -l debug")
    sys.exit(1)
