import logging
import os
import time

from logging.handlers import RotatingFileHandler
from flask import Flask, request, render_template, redirect, url_for

import tasks
from config import config, Config

flask = Flask(__name__)
env = os.environ['FLOWBOT_ENV']
flask.config.from_object(config[env])

formatter = logging.Formatter(
    "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
handler = RotatingFileHandler(flask.config['LOG_FILENAME'], maxBytes=10000000,
                              backupCount=5)
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)
flogger = flask.logger
flogger.addHandler(handler)

flogger.info("ENV : %s" % env)
flogger.info("FLASK CONFIG: %s" % flask.config)
flogger.info("Active Projects: %s" % flask.config['JIRA_PROJECTS'])

@flask.route('/', methods=['GET', 'POST'])
def index():
    return "404"

@flask.route('/webhook', methods=['POST'])
def webhook():
    project = request.args['project']
    # MAY NEED A WAY TO STOP CIRCULAR TRIGGERS
    tasks.query_jira.delay(project)
    return "OK"

@flask.route('/healthcheck', methods=['GET'])
def healthcheck():
    # MAY NEED A WAY TO STOP CIRCULAR TRIGGERS
    jql = 'project = JIRADM and status != Closed' # Jira Admin project - Ask # Eric Sebian
    flogger.debug("In HealthCheck!!")
    try:
        flogger.debug("Healthcheck jql = %s" % jql)
        res = tasks.test_query_jira_via_jql.delay(jql)
        max_checks = 50 # 5 seconds
        for ctr in range(1, max_checks+1):
            flogger.debug("Healthcheck Checking %s %i out of %i: %s" % (res, ctr, max_checks, res.status))
            if res.status != 'PENDING':
                break
            elif ctr == max_checks:
                return "NOT OK - Timeout"
            else:
                time.sleep(100.0 / 1000.0) # 1 tenth of a second
    except Exception as e:
        flogger.debug("Error with query = %s" % e)
        return "NOT OK - ", e
    try:
        flogger.debug("Healthcheck state: %s" % res.state)
        if res.successful():
            return "OK"
        else:
            return "NOT OK - result not successful"
    except Exception as e:
        flogger.debug("Error = %s" % e)
        return "NOT OK - when checking res.successful"


if __name__ == '__main__':

    tasks.query_jira.delay()
    flask.run(debug=True, host='0.0.0.0', port=5000)
