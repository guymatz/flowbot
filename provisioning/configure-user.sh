#!/usr/bin/env bash

#------------------------------------------------------------------------------
# Copy PyCharm configuration.
#------------------------------------------------------------------------------

cp -r $VAGRANT_SYNCHED_FOLDER/provisioning/pycharm/* ~/.PyCharm2016.1

#------------------------------------------------------------------------------
# Prepare variables for the Python virtual environment.
# See https://virtualenvwrapper.readthedocs.io/en/latest/index.html
#------------------------------------------------------------------------------

cat >> ~/.bashrc <<ECHO
# Configure virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$(dirname $VAGRANT_SYNCHED_FOLDER)
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/bin/virtualenvwrapper.sh

# Automatically activate virtualenv using name specified in the .venv file in
# the current directory, if there is one.
prompt() {
    if [ "\$PWD" != "\$MYOLDPWD" ]; then
        MYOLDPWD="\$PWD"
        test -f .venv && workon \$(cat .venv)
    fi
}

# Support for bash
PROMPT_COMMAND='prompt'
ECHO

source ~/.bashrc

#------------------------------------------------------------------------------
# Create the Python virtual environment.
#------------------------------------------------------------------------------

# Place the project name in the .venv file for the 'prompt' function above.
echo $PROJECT_NAME > $VAGRANT_SYNCHED_FOLDER/.venv

# Create the virtualenvwrapper project, which creates the 
mkproject --force $PROJECT_NAME
pwd
pip install -r requirements-dev.txt
pip install -r requirements.txt

#------------------------------------------------------------------------------
# Initialize the application database, if necessary.
#------------------------------------------------------------------------------

test -e migrations || (python manage.py db init; python manage.py db upgrade)

