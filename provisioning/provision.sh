#!/usr/bin/env bash

# TODO Remove this once base image is corrected.
rm /etc/yum.repos.d/*.repo

# Add custom yum repositories
cp $VAGRANT_SYNCHED_FOLDER/provisioning/*.repo /etc/yum.repos.d/

# Install Python 3.4, pip, and virtualenvwrapper
yum install -y python34u python34u-pip
pip3 install --upgrade pip
pip3 install virtualenvwrapper

# Install redis, set it to start at boot, and start it now.
yum install -y redis
chkconfig redis on
/etc/init.d/redis start

# Perform provisioning that must be performed as 'vagrant' user
su -c "$VAGRANT_SYNCHED_FOLDER/provisioning/configure-user.sh" vagrant

