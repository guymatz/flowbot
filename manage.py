#!/usr/bin/env python

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Server

from main import app, db, PreviousPost

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command("server", Server())
manager.add_command("db", MigrateCommand)


@manager.shell
def make_shell_context():
    return dict(app=app, db=db, PreviousPost=PreviousPost)


if __name__ == "__main__":
    manager.run()
