import os
import yaml
import sys

def load_active_projects(flowbot_env):
    #print("Flowbot_Env = %s" % flowbot_env)
    file = (os.path.join('config', flowbot_env, "projects.yml"))
    #print("Loading from %s" % file)
    try:
        with open(file) as yfile:
            projects = yaml.load(yfile)
    except Exception as e:
        #print("ERROR loading projects file: ", e)
        sys.exit()
    #print("All Projects: %s" % projects)
    active_projects = {}
    for proj in projects:
        active_projects[proj] = [projects[proj][x] for x in
                                 range(len(projects[proj])) if
                                 projects[proj][x]['active']]
    #print("Active Projects: %s" % active_projects)
    return active_projects

def load_active_scheduled_jobs(flowbot_env):
    #print("Flowbot_Env = %s" % flowbot_env)
    file = (os.path.join('config', flowbot_env, "jobs.yml"))
    try:
        with open(file) as yfile:
            jobs = yaml.load(yfile)
    except Exception as e:
        #print("ERROR loading jobs file: ", e)
        sys.exit()
    # There might ba a better way to do this with a dict comprehension
    # inside a list comprehension, or by making the yaml data better
    #print("jobs : %s" % jobs)
    active_jobs = {}
    for title, scheduled_job in jobs.items():
        if scheduled_job['active']:
            active_jobs[title] = scheduled_job
    return active_jobs

class Config(object):
    DEBUG = True
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///database.db'
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SAVE_POSTS = False
    BMS_URL = 'UNKNOWN'
    LOG_FILENAME = 'flowbot.log'
    BROKER_URL = os.getenv('BROKER_URL') or os.getenv('CELERY_BROKER_URL') or 'redis://127.0.0.1:6379/0'
    CELERY_BROKER_URL = BROKER_URL
    CELERY_RESULT_BACKEND = os.getenv('CELERY_RESULT_BACKEND') or BROKER_URL
    CELERY_ENABLE_UTC = False
    if os.path.exists('.env'):
        #print('Importing environment from .env...')
        for line in open('.env'):
            k, v = line.strip().split('=')
            os.environ[k.strip()] = v.strip()
    JIRA_USER = os.environ.get('JIRA_USER')
    JIRA_PASS = os.environ.get('JIRA_PASS')
    JIRA_SERVER = os.environ.get('JIRA_SERVER')
    # SAP vars
    SAP_USER = 'JIRA_USER'
    SAP_PASSWD = 'Jcrew@123'
    JENKINS_URL = 'http://nyc-wdevtool-02:8080'
    # This duplication is so stupid
    JENKINS_DEPLOY_TOKEN = {
        'UAT': 'igchogegUdmoddOonveebjaiHotjiBlu',
        'GOLD': 'igchogegUdmoddOonveebjaiHotjiBlu',
        'ZANKER': 'igchogegUdmoddOonveebjaiHotjiBlu',
        'PRODUCTION': 'ellUdHuctOdOpweOwyahejParofjeth4',
    }
    JENKINS_PROMOTE_TOKEN = {
        'UAT': 'igchogegUdmoddOonveebjaiHotjiBlu',
        'GOLD': 'igchogegUdmoddOonveebjaiHotjiBlu',
        'ZANKER': 'igchogegUdmoddOonveebjaiHotjiBlu',
        'PRODUCTION': 'ellUdHuctOdOpweOwyahejParofjeth4',
    }

class QaConfig(Config):
    DEBUG = True
    FLOWBOT_ENV = 'qa'
    #print("In %s" % FLOWBOT_ENV)
    BMS_URL = {
        'JCrew': 'http://bwi-jbmacs-i01:8085',
        'Factory': 'http://bwi-jbmacs-i01:8085',
        'Madewell1937': 'http://bwi-mbmacs-i01:8085'
    }
    if os.environ['FLOWBOT_ENV'] == FLOWBOT_ENV:
        JIRA_PROJECTS = load_active_projects(FLOWBOT_ENV)
        SCHEDULED_JOBS = load_active_scheduled_jobs(FLOWBOT_ENV)
    # SAP vars
    SAP_COMP = {
        'ECC': {
            'SAP_SYSID': 'QAX',
            'SAP_MSHOST': 'qaxci',
            'SAP_MSSERV': '3601',
            'SAP_CLIENT': '200',
            'SAP_GROUP': 'R3'
        },
        'BW': {
            'SAP_SYSID': 'BHQ',
            'SAP_MSHOST': 'bhqci',
            'SAP_MSSERV': '3611',
            'SAP_CLIENT': '010',
            'SAP_GROUP': 'BW'
        },
        'HR': {
            'SAP_SYSID': 'HRQ',
            'SAP_MSHOST': 'hrqci',
            'SAP_MSSERV': '3621',
            'SAP_CLIENT': '200',
            'SAP_GROUP': 'HR'
        },
    }

class ProductionConfig(Config):
    FLOWBOT_ENV = 'production'
    #print("In %s" % FLOWBOT_ENV)
    BMS_URL = {
        'JCrew': 'http://bwi-jbmacs-g01:8085',
        'Factory': 'http://bwi-jbmacs-g01:8085',
        'Madewell1937': 'http://bwi-mbmacs-g01:8085'
    }
    if os.environ['FLOWBOT_ENV'] == FLOWBOT_ENV:
        JIRA_PROJECTS = load_active_projects(FLOWBOT_ENV)
        SCHEDULED_JOBS = load_active_scheduled_jobs(FLOWBOT_ENV)
    # SAP vars
    SAP_PASSWD = 'Jcr3w@77o'
    SAP_COMP = {
        'ECC': {
            'SAP_SYSID': 'PRD',
            'SAP_MSHOST': 'prdci',
            'SAP_MSSERV': '3601',
            'SAP_CLIENT': '200',
            'SAP_GROUP': 'R3'
        },
        'BW': {
            'SAP_SYSID': 'BHP',
            'SAP_MSHOST': 'bhpci',
            'SAP_MSSERV': '3611',
            'SAP_CLIENT': '010',
            'SAP_GROUP': 'BW'
        },
        'HR': {
            'SAP_SYSID': 'HRP',
            'SAP_MSHOST': 'hrpci',
            'SAP_MSSERV': '3621',
            'SAP_CLIENT': '200',
            'SAP_GROUP': 'HR'
        },
        'GRC': {
            'SAP_SYSID': 'PGR',
            'SAP_MSHOST': 'pgrci',
            'SAP_MSSERV': '3676',
            'SAP_CLIENT': '010',
            'SAP_GROUP': 'GRC'
        }
    }

class DevConfig(Config):
    DEBUG = True
    FLOWBOT_ENV = 'dev'
    #print("In %s" % FLOWBOT_ENV)
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///database.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SAVE_POSTS = True
    BMS_URL = {
        'JCrew': 'http://nyc-cbmweb-d08:8085',
        'Factory': 'http://nyc-cbmweb-d08:8085',
        'Madewell1937': 'http://nyc-cbmweb-d08:8085'
    }
    if os.environ['FLOWBOT_ENV'] == FLOWBOT_ENV:
        JIRA_PROJECTS = load_active_projects(FLOWBOT_ENV)
        SCHEDULED_JOBS = load_active_scheduled_jobs(FLOWBOT_ENV)
    SAP_COMP = {
        'ECC': {
            'SAP_SYSID': 'DEV',
            'SAP_MSHOST': 'devci',
            'SAP_MSSERV': '3601',
            'SAP_CLIENT': '010',
            'SAP_GROUP': 'R3'
        },
        'BW': {
            'SAP_SYSID': 'BHD',
            'SAP_MSHOST': 'bhdci',
            'SAP_MSSERV': '3611',
            'SAP_CLIENT': '010',
            'SAP_GROUP': 'BW'
        },
        'GRC': {
            'SAP_SYSID': 'SGR',
            'SAP_MSHOST': 'sgrci',
            'SAP_MSSERV': '3676',
            'SAP_CLIENT': '010',
            'SAP_GROUP': 'GRC'
        },
        'HR': {
            'SAP_SYSID': 'HRD',
            'SAP_MSHOST': 'hrdci',
            'SAP_MSSERV': '3621',
            'SAP_CLIENT': '010',
            'SAP_GROUP': 'HR'
        },
    }


config = {
    'dev': DevConfig,
    'qa': QaConfig,
    'production': ProductionConfig,
    'default': DevConfig
}
