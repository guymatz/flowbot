import re
import yaml

class Process(object):

  def __init__(self, jira, issue, celery_conf):
    self.jira = jira
    self.issue = issue
    self.celery_conf = celery_conf
    self.notification_directive = 'PLEASE RESPOND TO THIS EMAIL BY REPLYING WITH EITHER "approved" OR "denied". ' \
                                  'Or you can log in to the JIRA application.'
    self.all_fields = None

  def process_attachment(self):
    raise ValueError("Please create process_attachment in your sub-class")

  def flowbot_updated(self):
    return bool(self.issue.changelog.histories[-1].author.key.startswith('flowbot'))

  def get_project(self):
    return self.jira.project(self.issue.fields.project.id)

  def get_all_fields(self):
      """" get all field names from jira, but only if we already haven't set it"""
      if self.all_fields:
          return
      else:
          self.all_fields = self.jira.fields()

  def get_field_id_by_name(self, name):
      """ Returns a field ID given a field name
      e.g. get_field_id_by_name('toggles') -> 'customfield_24700'
      """
      self.get_all_fields()
      return [field['id'] for field in self.all_fields if field['name'] == name][0]

  def get_field_name_by_id(self, id):
      """ Returns a field name given a field ID
      e.g. get_field_id_by_name('customfield_24700') -> 'toggles'
      """
      self.get_all_fields()
      return [field['name'] for field in self.all_fields if field['id'] == id][0]

  # Relies on brand being in the project description in yaml format
  def get_brand(self):
    project_desc = self.get_project().description
    try:
      project_yaml = yaml.load(project_desc)
      return project_yaml['brand']
    except Exception as e:
      return 'Brand UNKNOWN:  Please verify brand is in the project description'

  def get_status(self):
    return self.issue.fields.status.name

  def get_description(self):
      return self.issue.fields.description

  def get_event(self):
    return self.payload.get('issue_event_type_name')

  def issue_updated_by_flowbot(self):
    return self.issue_updated() == 'issue_updated' and self.flowbot_updated()

  def issue_commented(self):
    return len(self.issue.fields.comment.comments) > 0

  def issue_commented_by_flowbot(self):
    return bool(self.payload.get('comment')) and self.flowbot_updated()

  def get_latest_attachment(self):
    try:
      attachments = self.issue.fields.attachment
      attachments.sort(key=lambda a: a.created)
      if len(attachments) > 0:
        latest =  attachments[-1]
        # print("I see attachment %s, created %s: " % (latest.filename, latest.created))
        self.attachment = { 'filename': latest.filename,
                          'filedata': latest.get()
                          }
      else:
        self.attachment = None
      return
    except Exception as e:
      # print("Could not get attachment: ", e)
      raise e

  def get_jira_id(self):
    return self.issue.key

  def status_has_changed(self):
    change = self.issue.changelog.histories[-1].items[-1]
    if change.field == 'status':
      return change.fromString != change.toString
    else:
      return False

  def get_operational_approver(self):
    #print("In get_operational_approver; %s" % self.issue.fields.customfield_19008.name)
    return self.issue.fields.customfield_19008.name

  def get_last_comment(self):
    # print("In get_last_comment")
    if self.issue_commented():
      return self.issue.fields.comment.comments[-1]
    # print("In get_last_comment, about to return None")
    return None

  def get_last_comment_body(self):
    # print("In get_last_comment_body")
    c = self.get_last_comment()
    if c:
      return c.body.strip()
    return ''

  def get_field_id_value(self, field):
      return getattr(self.issue.fields, field)

  def get_field_name_value(self, field):
      id = next(x['id'] for x in fields if x['name'] == field)
      return self.get_field_id_value(id)

  def transition_issue(self, new_status, comment=None):
    print("Changing issue %s to status %s with comment '%s'" % (self.issue.key, new_status, comment))
    try:
        tran_id = self.jira.find_transitionid_by_name(issue=self.issue.key, transition_name=new_status)
        print("Transition ID %s" % tran_id)
    except Exception as e:
        print("Could not get transition ID: %s" % e)
        raise ValueError("No transition ID available")
    if tran_id:
        try:
            self.jira.transition_issue(self.issue, tran_id, comment=comment)
        except Exception as e:
            print("Could not transition %s: %s" (self.issue.key, e))
            raise Error("Unable to transition issue: %s" % e)
    else:
        print("Could not get transition ID for issue %s" % self.issue.key)
        raise ValueError("No transition ID exists")

  # Only checking the last comment.  this could bite us!!
  def is_denied_by_approver(self):
      # print("In is_denied_by_approver")
      c = self.get_last_comment()
      if c:
          body = c.body.strip()
          author = c.author.name
          m = re.search('(den(ied|y)|approv(ed|al))', body, flags=re.IGNORECASE | re.MULTILINE | re.DOTALL)
          if m:
              # print("In deny check: %s" % m.group())
              return author == self.get_operational_approver() and m.group().lower().startswith('den')
          else:
              return None
      else:
          return None

  def last_comment_author(self):
      # print("In last_comment_author")
      c = self.get_last_comment()
      # print("In last_comment_author, Comment is %s" % c)
      if c:
          return c.author.displayName
      else:
          return None

  def is_approved(self, acceptance_regex):
      c = self.get_last_comment()
      if c:
          body = c.body.strip()
          print("In is_approved, body = %s" % body)
          # Check for either approved or implemented
          m = re.search('^(%s)' % acceptance_regex, body, flags=re.IGNORECASE | re.MULTILINE | re.DOTALL)
          # print("In is_approved_by_approver, m = %s" % m)
          if m:
              return True
      return False

  def is_denied(self, acceptance_regex):
      c = self.get_last_comment()
      if c:
          body = c.body.strip()
          #print("In is_denied, body = %s" % body)
          m = re.search('^(%s)' % acceptance_regex, body, flags=re.IGNORECASE | re.MULTILINE | re.DOTALL)
          #print("In is_denied, m = %s" % m)
          if m:
              return True
      return False

  def is_approved_by_user(self, approver):
      print("In is_approved_by_user: comparing %s and %s" % (self.last_comment_author(), approver ))
      return self.last_comment_author() == approver and self.is_approved('approv(e|al)')

  def is_completed_by_user(self, approver):
      print("In is_completed_by_user: comparing %s and %s" % (self.last_comment_author(), approver ))
      return self.last_comment_author() == approver and self.is_approved('implement|complet(ed)')

  def is_denied_by_user(self, approver):
      return self.last_comment_author() == approver and self.is_denied('den(ied|y)')

  def is_approved_by_group_member(self, group):
      print("In is_approved_by_group_member . . .")
      group_members = self.jira.group_members(group)
      group_member_names = [ group_members[x]['fullname'] for x in group_members ]
      print("In is_approved_by_group_member: comparing %s and %s" % (self.last_comment_author(), group_member_names ))
      return self.last_comment_author() in group_member_names and self.is_approved('(implement|complet(ed))|approv(e|al)')

  def is_denied_by_group_member(self, group):
      print("In is_denied_by_group_member . . .")
      group_members = self.jira.group_members(group)
      group_member_names = [ group_members[x]['fullname'] for x in group_members ]
      #print("In is_denied_by_group_member: Looking for '%s' in %s" % (self.last_comment_author(), group_member_names ))
      is_member =  self.last_comment_author() in group_member_names
      #print("In is_denied_by_group_member: self.last_comment_author() in group_member_names = %s" % is_member)
      #print("In is_denied_by_group_member: self.denied = %s" % self.is_denied('den(ied|y)|cancel'))
      return is_member and self.is_denied('den(ied|y)|cancel')

  def verify_email_approval(self, approver):
    print("In verify_email_approval")
    approved = False
    denied = False
    name_fields = { f['name']:f['id'] for f in self.jira.fields() }
    # first check if the approver is a field
    if getattr(self.issue.fields, name_fields[approver]):
        approver_username = ''
        try:
            approver_username = getattr(self.issue.fields, name_fields[approver]).displayName
        except Exception as e:
            print("Error getting approver_displayName: %s" % e)
            raise e
        print("Got approver_displayName: %s" % approver_username)
        if self.is_approved_by_user(approver_username):
            approved = True
        elif self.is_denied_by_user(approver_username):
            denied = True
    # next check if the approver is a group
    elif self.jira.group_members(approver):
        # print("In elif")
        approver_groupname = approver
        if self.is_approved_by_group_member(approver_groupname):
            approved = True
        elif self.is_denied_by_group_member(approver_groupname):
            denied = True
    # print("after 1st if")
    if approved:
      try:
        self.transition_issue('Flowbot Approved', comment=None)
        print("Ticket approved!")
      except Exception as e:
        raise ValueError("I think I should have approved that, but couldn't because ", e)
    elif denied:
      try:
        self.transition_issue('Flowbot Denied', comment=None)
        print("Ticket Denied!  In your face!!")
      except Exception as e:
        print("What should I do now!? ", e)
        raise ValueError("I think I should have denied that, but couldn't because ", e)
    else:
        print("I see a comment, but it is not an approve/deny by the appropriate Approver")

  def verify_email_completion(self, approver):
    # print("In verify_email_approval")
    if self.is_approved_by_group_member(approver):
      try:
        self.transition_issue('Flowbot Implemented', comment=None)
        print("Ticket implemented!")
      except Exception as e:
        raise ValueError("I think I should have implemented that, but couldn't because ", e)
    elif self.is_denied_by_group_member(approver):
      try:
        self.transition_issue('Flowbot Cancelled', comment=None)
        print("Ticket Cancelled!  In your face!!")
      except Exception as e:
        print("What should I do now!? ", e)
        raise ValueError("I think I should have denied that, but couldn't because ", e)
    else:
        print("I see a comment, but it is not a implemented/cancelled by Verizon")

  def update_synopsis(self):
    print("Updating synopsis for %s" % self.issue.key)
    try:
      if not self.synopsis_fields:
        raise ValueError("No synopsis fields defined for this object type")
      new_synopsis = self.synopsis_fields.pop('issuetype')
      for field_name, field_value in self.synopsis_fields.items():
        new_synopsis = "%s\n%s: %s" % (new_synopsis, field_name, field_value)
      new_synopsis = ('%s\n\n%s' % (new_synopsis, self.notification_directive))
      new_synopsis = ("%s\n%s" % (new_synopsis, self.get_last_comment_body()))
    except Exception as e:
      raise e
    # custom field 19724 is Synopsis
    try:
      self.issue.update(fields={'customfield_19724': new_synopsis})
      return
    except Exception as e:
      print("Could not set synopsis: ", e.text)
      raise e

  def add_last_comment_to_synopsis(self):
    try:
      synopsis = self.issue.fields.customfield_19724
      new_synopsis = synopsis + "\nLast Comment: " + self.get_last_comment_body()
      self.issue.update(fields={'customfield_19724': new_synopsis})
    except Exception as e:
      print("Could not add last comment to synopsis: ", e)
      raise e

  def remove_watchers(self):
     try:
       watchers = self.jira.watchers(self.issue.id)
       print("watcher in %s = %s . . . " % (self.issue.key, watchers.watchers))
       print("Now sleeping for 10 seconds")
       #time.sleep(10)
     except Exception as e:
       print("Could not get watchers: ", e)
       raise e
     try:
       for user in watchers.watchers:
         print("Removing %s . . . " % user.key)
         resp = watchers.delete(user.key)
         print("Response from removal: %s" % resp)
     except Exception as e:
       print("Could not delete watcher: ", e)
       raise e

  def create_subtasks(self, subtask_types):
    print("subtasks: %s" % subtask_types)
    for subtask_type in subtask_types:
      if subtask_type in [x.fields.issuetype.name for x in self.issue.fields.subtasks]:
        print("NOT creating subtask since it already exists")
        continue
      try:
        fields = {'issuetype': {'name': subtask_type},
                  'project': {'key': self.issue.fields.project.key},
                  'summary': self.issue.fields.summary,
                  'parent': {'id': self.issue.key}
                  }
        self.jira.create_issue(fields=fields)
      except Exception as e:
        print("Could not create subtask of %s for %s: %s\nwith fields = %s" % (subtask_type, self.issue.key, e, str(fields)))
        raise e

  def add_comment(self, comment):
    try:
      self.jira.add_comment(self.issue, comment)
    except Exception as e:
      print("Caught exception in add_comment for issue %s: %s" % (self.issue, str(e)))
      raise e
    
  def aggregate_child_fields(self, field):
      """ Aggregate fields from a child into the same field in the parent """
      try:
          field_id = self.get_field_id_by_name(field)
      except Exception as e:
          raise ValueEror("Could not find id for field %s: %s" % (field, str(e)))
      agg = ""
      for child in self.issue.fields.subtasks:
          child_issue = self.jira.issue(child.key)
          if child_issue.fields.status.name == 'Closed':
              agg = "%s\n%s" % (agg, getattr(child_issue.fields, field_id))
      self.issue.update({field_id:agg})
