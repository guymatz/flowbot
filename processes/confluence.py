import requests
from html import escape
from time import gmtime, strftime
import json
import os
import sys

class Confluence(object):

    def __init__(self, url, username, password):
        """

        :param url: URL of the Confluence server
        :param username: username to use for authentication
        :param password: password to use for authentication
        :return: Confluence -- an instance to a Confluence object.
        :raises: EnvironmentError

        """
        print("__init__")
        self.server = requests.Session()
        self.server.auth = (username, password)
        self.base = url + '/rest/api'

    def getPage(self, space, title, expand='history,version,space'):
        """
        Returns a page object as a dictionary.

        :param space:
        :param title:
        :return: dictionary. result['content'] contains the body of the page.
        """
        print("getPage: %s %s" % (space, title))
        url = self.base + '/content'
        data = {'spaceKey':space, 'title':title, 'expand': expand}
        #print("url: %s" % (url))
        #print("data: %s" % (data))
        page = self.server.get(url, params=data)
        print("getPage: %s" % (page))
        return page

    def getPageVersion(self, space, title):
        """
        Retuns the numeric version of a confluence page.

        :param space:
        :param title:
        :return: Integer: page version
        """
        print("getPageVersion")
        page = self.getPage(space, title)
        print("getPageVersion: %s" % page)
        return page.json()['results'][0]['version']['number']

    def getPageId(self, space, title):
        """
        Retuns the numeric id of a confluence page.

        :param space:
        :param title:
        :return: Integer: page numeric id
        """
        print("getPageId: %s %s" % (space, title))
        page = self.getPage(space, title)
        return page.json()['results'][0]['id']

    def getPageVersionFromPage(self, page):
        """
        Retuns the numeric version of a confluence page.

        :param page object:
        :return: Integer: page version
        """
        print("getPageVersionFromPage")
        #print("Page: %s" % page)
        #print("Page.Json: %s" % page.json())
        #print("Page.Json: results = %s" % page.json()['results'][0])
        try:
            ver = page.json()['results'][0]['version']['number']
            print("getPageVersionFromPage: %s" % ver)
            return ver
        except Exception as e:
            print("Could not get Page Version!!")
            return 0

    def getPageIdFromPage(self, page):
        """
        Retuns the numeric id of a confluence page.

        :param page object:
        :return: Integer: page numeric id
        """
        print("getPageIdFromPage: %s" % page)
        try:
            id = page.json()['results'][0]['id']
            print("getPageIdFromPage: ID = %s" % id)
            return id
        except Exception as e:
            print("Could not get Page ID!!")
            return 0

    def storePageContent(self, space, title, content):
        """
        Modifies the content of a Confluence page.

        :param space:
        :param title:
        :param content:
        :return: bool: True if succeeded
        """
        print("storePageContent")
        page = self.getPage(space, title)
        page_version = self.getPageVersionFromPage(page)
        new_page_version = page_version + 1
        page_id = self.getPageIdFromPage(page)

        resp = None
        try:
            url = self.base + '/content/' + page_id
            body = json.dumps(json.loads(content), indent=4, sort_keys=True)
            payload = {'version': {"number": new_page_version},
                       'title':title,
                       'type': 'page',
                       'body': {"storage":
                                    {"value": '<pre>%s</pre>' % escape(body),
                                    "representation": "storage"
                                     }
                                }
            }
        except Exception as e:
            print("Problem setting payload: %s" % e)
            return
        #print("payload = %s" % payload)
        try:
            resp = self.server.put(url, json=payload)
        except Exception as e:
            print("Problem sending payload to confluence: %s" % e)
            return
        print("resp = %s: %s" % (resp, resp.text))
        return resp
