from processes.issuetypes import IssueType


class GlobalNavUpdate(IssueType):

  def __init__(self):
    super(GlobalNavUpdate, self).__init__()

  def process(self, issue):
    print("Processing GlobalNavUpdate")
