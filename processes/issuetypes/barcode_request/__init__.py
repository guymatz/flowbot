from processes.issuetypes import IssueType


class BarcodeRequest(IssueType):

  def __init__(self, jira, issue, celery_conf):
    super(BarcodeRequest, self).__init__(jira, issue, celery_conf)
    print("HI from BarcodeRequest!!!")

  def process(self, issue):
    print("Processing BarcodeRequest")
