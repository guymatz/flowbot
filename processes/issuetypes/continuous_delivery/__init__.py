from processes.issuetypes import IssueType


class ContinuousDelivery(IssueType):

  def __init__(self):
    super(ContinuousDelivery, self).__init__()

  def process(self, issue):
    print("Processing ContinuousDelivery")