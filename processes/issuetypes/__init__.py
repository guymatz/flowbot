import re
import yaml
from processes import Process

class IssueType(Process):

  def __init__(self, jira, issue, celery_conf):
    super(IssueType, self).__init__(jira, issue, celery_conf)

  def process_attachment(self):
    raise ValueError("Please create process_attachment in your sub-class")
