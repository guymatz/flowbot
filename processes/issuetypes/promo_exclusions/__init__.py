import json

import xlrd

from processes.issuetypes import IssueType
from processes.bmi import Import


class PromoExclusions(IssueType):
    def __init__(self, jira, issue, celery_conf):
        super(PromoExclusions, self).__init__(jira, issue, celery_conf)
        print("HI from PromoExclusions!!!")
        self.jira_id = self.get_jira_id()
        self.brand = self.get_brand()
        # Hitting the BM REST API
        print("Brand: %s" % self.brand)
        #print("Celery Config BMS_URL: %s" % str(celery_conf.conf['BMS_URL'][self.brand]))
        self.endpoint = celery_conf['BMS_URL'][self.brand] + '/products/exclusions'
        self.bm_object = 'productCodes'

    def process_attachment(self):
        try:
            self.get_latest_attachment()
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="Please attach Spreadsheet")
            return
        # Need to check if SS exists.  Better way to combine this and above?
        if not self.attachment:
            self.transition_issue('Flowbot Denied', comment="Please attach Spreadsheet")
            return
        try:
            self.process_spreadsheet()
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="There was an issue with the SS:\n%s" % e)
            return

    def reset_exclusions(self):
        try:
            print("Creating Import object . . .")
            bmi = Import(self.issue.key, self.endpoint)
        except Exception as e:
            print("Oops in attempt to create Import object for %s!!: %s" + (self.issue.key, str(e)))
            self.transition_issue('Flowbot Denied', comment="Something went wrong resetting exclusions (1):\n" +
                                   "Please contact your Jira Administrator:\n%s" % e)
            return
        try:
            print("Running reset on Import object . . .")
            ex_list = {'brand':self.get_brand(), self.bm_object: []}
            resp = bmi.run(ex_list)
        except Exception as e:
            print("Oops in attempt to reset exclusions for %s!!: %s" % (self.issue.key, str(e)))
            self.transition_issue('Flowbot Denied', comment="Something went wrong resetting exclusions (2):\n" +
                                  "Please contact your Jira Administrator:\n%s" % e)
            return
        try:
            if resp.ok:
                self.transition_issue('Flowbot Approved', comment="PromoExclusions Successfully Reset")
            elif 'errorMessages' in resp.text:
                jtext = json.loads(resp.text)
                print("D'oh: " + "\n".join(jtext['errorMessages']))
                self.transition_issue('Flowbot Denied', comment="\n".join(jtext['errorMessages']))
            else:
                self.transition_issue('Flowbot Denied', comment="Something went wrong resetting exclusions (3):\n%s" % resp.text)
            return
        except Exception as e:
            print("Complaint from BM API!!: " + str(e))
            self.transition_issue('Flowbot Denied', comment="Something went wrong resetting exclusions (4):\n%s" % resp.text)
            return


    def process_spreadsheet(self):
        #wb = xlrd.open_workbook(self.issue.key, file_contents=self.attachment)
        try:
            wb = xlrd.open_workbook(file_contents=self.attachment['filedata'])
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="There was an issue opening the SS:\n%s" % e)
            return
        bmi = Import(self.issue.key, self.endpoint)
        exclusion_list = []
        bad_list = []
        # customfield_19502 is 'Exclude from Promo' radio button
        # to_exclude = self.issue.fields.customfield_19502.value
        try:
            for sheet in wb.sheets():
                print("I found a worksheet with %i rows" % sheet.nrows)
                if sheet.nrows == 0:
                    continue
                for row_num in range(sheet.nrows):
                    row = sheet.row(row_num)
                    if row[0].ctype == xlrd.XL_CELL_NUMBER:
                        prod_code = str(int(row[0].value)).zfill(5)
                    else:
                        prod_code = row[0].value
                    #if not prod_code.isalnum():
                    #    bad_list.append(prod_code)
                    exclusion_list.append(prod_code)
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="There was an issue parsing the SS:\n%s" % e)
            return
        #if len(bad_list) > 0:
        #    raise ValueError("Bad Promo Codes: " + "\n".join(bad_list))
        print("about to hit %s with exclusion_list of len %i" % (self.endpoint, len(exclusion_list)))
        try:
            ex_list = {'brand':self.get_brand(), self.bm_object: exclusion_list}
            print("Ex List: " + json.dumps(ex_list))
            resp = bmi.run(ex_list)
        except Exception as e:
            print("Oops in first try of process_spreadsheet!!: " + str(e))
            self.transition_issue('Flowbot Denied', comment="Something went wrong setting exclusions (1):\n" +
                                                            "Please contact your Jira Administrator:\n%s" % e)
            return
        try:
            if resp.ok:
                self.transition_issue('Flowbot Approved', comment="%i PromoExclusions Successfully Processed" % len(exclusion_list))
            elif 'errorMessages' in resp.text:
                jtext = json.loads(resp.text)
                print("D'oh: " + "\n".join(jtext['errorMessages']))
                self.transition_issue('Flowbot Denied', comment="\n".join(jtext['errorMessages']))
            else:
                self.transition_issue('Flowbot Denied', comment="Something went wrong:\n%s" % resp.text)
        except Exception as e:
            print("Oops in second try of process_spreadsheet!!: " + str(e))
            self.transition_issue('Flowbot Denied', comment="Something went wrong setting exclusions (2):\n" +
                                                            "Please contact your Jira Administrator:\n%s" % e)
