from processes.issuetypes import IssueType
import os
import json
import xlrd
import re

class DeactivateUsers(IssueType):
    def __init__(self, jira, issue, celery_conf):
        super(DeactivateUsers, self).__init__(jira, issue, celery_conf)
        print("HI from DeactivateUsers!!!")
        self.jira_id = self.get_jira_id()

    def deactivate_users(self):
        self.process_description()

    def process_description(self):
        try:
            desc = self.get_description()
            if desc is None:
                self.transition_issue('Flowbot Denied', comment="Please add addresses to description")
            #  The email from info-sec comes in a goofy format.  I hope this works!
            #address_text = desc.split('\n\n')[1]
            addresses = desc.split('\n')
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="Error getting description:\n%s" % e)
        self.do_deactivation(addresses)

    def process_attachment(self):
        try:
            self.get_latest_attachment()
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="Please attach Spreadsheet")
        try:
            attachment_type = self.attachment['filename'].partition('.')[-1]
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="Could not get attachment type (extension)")
        if attachment_type == 'txt':
            try:
                addresses = self.get_list_from_textfile()
            except Exception as e:
                self.transition_issue('Flowbot Denied', comment="There was an issue parsing the textfile:\n%s" % e)
        elif attachment_type in ['xls', 'xlsx']:
            try:
                addresses = self.get_list_from_spreadsheet()
            except Exception as e:
                self.transition_issue('Flowbot Denied', comment="There was an issue parsing the SS:\n%s" % e)
        else:
            self.transition_issue('Flowbot Denied', comment="I do not know how to work with file extension: %s" % attachment_type)
        self.do_deactivation(addresses)

    def get_list_from_spreadsheet(self):
        wb = xlrd.open_workbook(file_contents=self.attachment)
        addresses = []
        for sheet in wb.sheets():
            print(" I found a worksheet with %i rows" % sheet.nrows)
            if sheet.nrows == 0:
                continue
            for row_num in range(sheet.nrows):
                row = sheet.row(row_num)
                email_addy = row[0].value
                addresses.append(email_addy)
        return addresses

    def get_list_from_textfile(self):
        data = self.attachment['filedata'].decode()    # file data comes as bytes
        addresses = data.split('\t\n')
        # probably need to remove email footer
        return addresses

    def do_deactivation(self, users_list):
        user_ctr = 0
        deactivated_users_ctr = 0

        email_regex = r'^[A-z0-9\.]*@jcrew.com'
        for email_addy in users_list:
            email_addy = email_addy.strip()
            if not re.match(email_regex, email_addy, flags=re.IGNORECASE ):
                continue
            try:
                user_ctr = user_ctr + 1
                user = self.jira.search_users(email_addy)
                #print("%s %i %s" % (email_addy, len(user), user))
                if len(user) == 0:
                    print("User does not exist: %s" % email_addy)
                    continue
                elif len(user) > 1:
                    print("User is not unique: %s" % email_addy)
                    continue
                else:
                    user_name = user[0].name
                print("Deactivating user %s <%s>" % (user_name, email_addy))
                self.jira.deactivate_user(user_name)
                deactivated_users_ctr =  deactivated_users_ctr + 1
            except Exception as e:
                print("Oops!  Error deactivating user %s: %s" % (email_addy, str(e)))
        try:
            comment = "Users Successfully Deactivated: %i out of %i " \
                      "addresses submitted" % ( deactivated_users_ctr, user_ctr)
            self.transition_issue('Flowbot Approved', comment=comment)
        except Exception as e:
            print("Oops!  Error transitioning issue: %s" % (str(e)))
