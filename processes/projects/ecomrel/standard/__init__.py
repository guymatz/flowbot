from processes.projects.ecomrel import Ecomrel

class Standard(Ecomrel):

  def __init__(self, jira, issue, celery_conf):
    super(Standard, self).__init__(jira, issue, celery_conf)
    print("Processing Standard Ecomrel issue %s!!!" % issue)
