from processes.projects.cms import Cms
from collections import OrderedDict

class Standard(Cms):

  def __init__(self, jira, issue, celery_conf):
    super(Standard, self).__init__(jira, issue, celery_conf)
    print("Processing Standard %s!!!" % issue)
    try:
      self.synopsis_fields['Brief'] = self.issue.fields.customfield_22002
      self.synopsis_fields['Reporter'] = self.issue.fields.reporter.displayName
      self.synopsis_fields['Operational Approver'] = self.issue.fields.customfield_19008.displayName
      self.synopsis_fields['Risk'] = self.issue.fields.customfield_19007.value
      self.synopsis_fields['Business Impact'] = self.issue.fields.customfield_19009
      self.synopsis_fields['Backout Plan'] = self.issue.fields.customfield_19006
      self.synopsis_fields['Downtime'] = self.issue.fields.customfield_19005
      self.synopsis_fields['Components'] = (', ').join([ x.name for x in
                                                         self.issue.fields.components ])
      self.synopsis_fields['Start Time'] = self.issue.fields.customfield_19004
      self.synopsis_fields['End Time'] = self.issue.fields.customfield_19500
    except Exception as e:
      print("Error setting synopsis: " + str(e))

    try:
        self.approvers = {
            'cto' : {
                      'name': self.issue.fields.customfield_22601.displayName,
                      'risk_factor': ['High', 'Medium']
            },
            'cio' : {
                      'name': self.issue.fields.customfield_22602.displayName,
                      'risk_factor': ['High']
            },
            'op' : {
                      'name': self.issue.fields.customfield_19008.displayName,
                      'risk_factor': ['High', 'Medium']
            },
            # This will be a list of users to check against
            'biz' : {
                      'name': self.issue.fields.customfield_22001,
                      'risk_factor': ['High', 'Medium']
            },
          }
    except Exception as e:
        print("Can't find a user", e)
