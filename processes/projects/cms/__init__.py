from processes.projects import Project
from collections import OrderedDict

class Cms(Project):
    def __init__(self, jira, issue, celery_conf):
        super(Cms, self).__init__(jira, issue, celery_conf)
        print("HI from Cms!!!")
        try:
            self.synopsis_fields = OrderedDict([
                ('issuetype', issue.fields.issuetype.name),
                ('Description', issue.fields.description),
                ('Bus. Justification', issue.fields.customfield_22102)
            ])
        except Exception as e:
            raise Error("Data looks bad: " + e)

        try:
            self.approvers = {
                'cto': {
                    'name': self.issue.fields.customfield_22601.displayName,
                    'risk_factor': ['High', 'Medium']
                },
                'cio': {
                    'name': self.issue.fields.customfield_22602.displayName,
                    'risk_factor': ['High']
                },
                'op': {
                    'name': self.issue.fields.customfield_19008.displayName,
                    'risk_factor': ['High', 'Medium', 'Low']
                },
                # This will be a list of users to check against
                'biz': {
                    'name': self.issue.fields.customfield_22001,
                    'risk_factor': ['High', 'Medium', 'Low']
                },
            }
        except Exception as e:
            print("Can't find a user", e)

    def get_approver_count_by_risk(self):
        """ We only want approvers who are required for this risk level """
        risk = self.issue.fields.customfield_19007.value
        return len([ x for x in self.approvers if risk in self.approvers[x]['risk_factor'] ])

    def get_approvers_by_risk(self):
        """ We only want approvers who are required for this risk level """
        risk = self.issue.fields.customfield_19007.value
        risk_approvers = []
        for approver in self.approvers:
            if risk in self.approvers[approver]['risk_factor']:
                if type(self.approvers[approver]['name']) is str:
                    risk_approvers.append(self.approvers[approver]['name'])
                elif type(self.approvers[approver]['name']) is list:
                    risk_approvers.extend(list(map(lambda x: x.displayName,  self.approvers[approver]['name'])))
                else:
                    print("In get_approvers_by_risk: what is %s" % self.approvers[approver]['name'])
        return risk_approvers

    def verify_email_approval(self):
        denied = False
        print("In verify_email_approval")
        approvers = self.get_approvers_by_risk()
        print("In verify_email_approval, approvers = %s" % approvers)
        approver_count = 0
        # first check if the approver is a field
        for approver in approvers:
            print("Checking for approval from %s" % approver)
            for c in self.issue.fields.comment.comments:
                body = c.body.strip().lower()
                print("author %s , body %s" % (c.author, body))
                if (c.author.displayName == approver) and body.startswith('approv'):
                    print("Approved by %s!" % c.author)
                    # An approver may be able to approve for multiple business types
                    approver_count = approver_count + approvers.count(c.author.displayName)
                    break
                elif c.author.displayName in approvers and body.startswith('den'):
                    print("Denied by %s!" % c.author)
                    denied = True
            if denied: break
        print("Approvals: %i" % approver_count)
        if approver_count >= self.get_approver_count_by_risk():
            try:
                self.transition_issue('Flowbot Approved', comment=None)
                print("Ticket approved!")
            except Exception as e:
                raise ValueError("I think I should have approved that, but couldn't because ", e)
        elif denied:
            print("here?")
            try:
                self.transition_issue('Flowbot Denied', comment=None)
                print("Ticket Denied!  In your face!!")
            except Exception as e:
                print("What should I do now!? ", e)
                raise ValueError("I think I should have denied that, but couldn't because ", e)
        else:
            print("I see a comment, but it is not an approve/deny by the appropriate Approver")

