from processes.projects.nacrf import Nacrf

class Standard(Nacrf):

  def __init__(self, jira, issue, celery_conf):
    super(Standard, self).__init__(jira, issue, celery_conf)
    print("Processing Standard %s!!!" % issue)
#    try:
#      self.synopsis_fields = OrderedDict([
#        ('issuetype', issue.fields.issuetype.name)
#      ])
#    except Exception as e:
#      raise Error("Data looks bad: " + e)
#    try:
#      self.synopsis_fields['Summary'] = self.issue.fields.summary
#      self.synopsis_fields['Reporter'] = self.issue.fields.reporter.displayName
#      self.synopsis_fields['Business Justification'] = self.issue.fields.customfield_22102
#      self.synopsis_fields['End Time'] = self.issue.fields.customfield_19500
#      self.synopsis_fields['Associated Project'] = self.issue.fields.customfield_22101
#      self.synopsis_fields['IP/Port Requests'] = self.issue.fields.customfield_22105
#    except Exception as e:
#      print("Error setting synopsis: " + str(e))
