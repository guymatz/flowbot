import xlrd
from processes.projects import Project
from collections import OrderedDict

class Nacrf(Project):
    def __init__(self, jira, issue, celery_conf):
        super(Nacrf, self).__init__(jira, issue, celery_conf)
        print("HI from Nacrf!!!")
        self.jira_id = self.get_jira_id()
        self.table_customfield_id = 'customfield_22105'
        self.notification_directive = 'PLEASE RESPOND TO THIS EMAIL BY REPLYING WITH EITHER "approved" OR "denied". ' \
                                      'Or you can log in to the JIRA application.'
        self.starting_row = 5
        try:
            self.synopsis_fields = OrderedDict([('issuetype', issue.fields.issuetype.name)])
        except Exception as e:
            raise ValueError("Data looks bad: " + str(e))
        try:
            self.synopsis_fields['Summary'] = self.issue.fields.summary
            self.synopsis_fields['Reporter'] = self.issue.fields.reporter.displayName
            self.synopsis_fields['Business Justification'] = self.issue.fields.customfield_22102
            self.synopsis_fields['End Time'] = self.issue.fields.customfield_19500
            self.synopsis_fields['Associated Project'] = self.issue.fields.customfield_22101
            self.synopsis_fields['IP/Port Requests'] = "\n    " + self.issue.fields.customfield_22105.replace('\n',
                                                                                                             '\n    ')
        except Exception as e:
            print("Error setting synopsis: " + str(e))

    def process_attachment(self):
        try:
            self.get_latest_attachment()
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="Please attach Spreadsheet")
            return
        # Need to check if SS exists.  Better way to combine this and above?
        if not self.attachment:
            self.transition_issue('Flowbot Denied', comment="Please attach Spreadsheet")
            return
        try:
            self.process_spreadsheet()
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="There was an issue with the SS:\n%s" % e)
            return

    def stringify_cell(self, xl_cell):
        if xl_cell.ctype == xlrd.XL_CELL_NUMBER:
            return str(int(xl_cell.value))
        else:
            return xl_cell.value

    def process_spreadsheet(self):
        try:
            wb = xlrd.open_workbook(file_contents=self.attachment['filedata'])
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="There was an issue opening the SS:\n%s" % e)
            return
        nacrf_table = []
        # customfield_19502 is 'Exclude from Promo' radio button
        # to_exclude = self.issue.fields.customfield_19502.value
        try:
            for sheet in wb.sheets():
                print("I found a worksheet with %i rows" % sheet.nrows)
                if sheet.nrows == 0:
                    continue
                separator = '||'   # Jira markup separator for table headings
                for row_num in range(self.starting_row, sheet.nrows):
                    row = sheet.row(row_num)
                    row_str = [ self.stringify_cell(c) if c.value != '' else ' ' for c in row ]
                    nacrf_entry = separator + separator.join(row_str) + separator
                    nacrf_table.append(nacrf_entry)
                    separator = '|' # Jira markup separator for table rows
        except Exception as e:
            self.transition_issue('Flowbot Denied', comment="There was an issue parsing the SS:\n%s" % e)
            return
        print("about to set %s with %i entries" % (self.table_customfield_id, len(nacrf_table)))
        try:
            self.issue.update(customfield_22105='\n'.join(nacrf_table))
        except Exception as e:
            print("Oops in attempt to update IP table: " + str(e))
            self.transition_issue('Flowbot Denied', comment="Something went wrong updating the IP Table\n" +
                                                            "Please contact your Jira Administrator:\n%s" % e)
            return
