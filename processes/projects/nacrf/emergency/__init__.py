from processes.projects.nacrf import Nacrf

class Emergency(Nacrf):

  def __init__(self, jira, issue, celery_conf):
    super(Emergency, self).__init__(jira, issue, celery_conf)
    print("Processing Emergency %s!!!" % issue)
    # TODO - Is there a better way to do this?
    if self.issue.fields.status.name == 'Pending Verizon Update':
      self.notification_directive = 'PLEASE RESPOND TO THIS EMAIL BY REPLYING WITH EITHER "completed" OR "denied".'
#    try:
#      self.synopsis_fields = OrderedDict([
#        ('issuetype', issue.fields.issuetype.name)
#      ])
#    except Exception as e:
#      raise Error("Data looks bad: " + e)
    try:
      self.synopsis_fields['Operational Approver'] = self.issue.fields.customfield_19008.displayName
    except Exception as e:
      print("Error setting synopsis: " + str(e))
    print("End of Emergency ")
