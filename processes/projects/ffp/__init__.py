from processes.projects import Project
from pyrfc import Connection


class Ffp(Project):
    '''Handles FFP'''
    def __init__(self, jira, issue, celery_config):
        super(Ffp, self).__init__(jira, issue, celery_config)

    def verify_email_approval(self, approver):
        approval = super(Ffp, self).verify_email_approval(approver)
        if approval.lower().contains('approv'):
            self.approve_ticket()
            self.unlock_sap()
        elif approval.lower().contains('deny'):
            self.deny_ticket()

    def change_sap_permissions(self, sap_function):
        # Firefighter ID
        ffp_user = self.issue.fields.customfield_23500
        if ffp_user is None:
            print("Could not find a ffp_user in %s" % self.issue.key)
            self.transition_issue("Flowbot Denied",
                                  comment="Please add an FFP ID")
            return
        print("About to call %s for %s" % (sap_function, ffp_user))
        resp = None
        connection_specs = {'sysid': self.SAP_SYSID, 'mshost': self.SAP_MSHOST,
            'msserv': self.SAP_MSSERV, 'group': self.SAP_GROUP,
            'client': self.SAP_CLIENT, 'user': self.SAP_USER,
            'passwd': self.SAP_PASSWD}
        try:
            # print("In change_sap_permission, connection_specs = %s" %
            # connection_specs)
            with Connection(**connection_specs) as conn:
                # print("In change_sap_permission, conn = %s" % conn)
                resp = conn.call(sap_function, USERNAME=ffp_user)
        except Exception as e:
            print("What's up with that!", e)
            print("About to transition with comment: %s" % str(e))
            self.transition_issue("Flowbot Denied", comment=str(e))
            raise e
        # print("RESP[0][MESS] = %s" % resp['RETURN'][0]['MESSAGE'])
        if 'does not exist' in resp['RETURN'][0]['MESSAGE']:
            self.transition_issue("Flowbot Denied",
                                  comment=resp['RETURN'][0]['MESSAGE'])
            raise ValueError(resp['RETURN'][0]['MESSAGE'])
            # print("Response = %s" % resp)

    def unlock_sap(self):
        try:
            self.change_sap_permissions('BAPI_USER_UNLOCK')
        except ValueError as ve:
            print("Received ValueError in unlock_sap: %s" % ve)
            return
        except Exception as e:
            print("Not transitioning ticket %s" % self.issue.key)
            return
        self.transition_issue("Flowbot Access Granted")

    def expire_sap_access(self):
        try:
            self.lock_sap()
        except ValueError as ve:
            print("Received ValueError in expire_sap_access: %s" % ve)
            return
        except Exception as e:
            print("In expire_sap_access: What's up with that!", e)
            return
        self.transition_issue("Flowbot Access Expired")

    def cancel_sap_access(self):
        try:
            self.lock_sap()
        except ValueError as ve:
            print("Received ValueError in cancel_sap_access: %s" % ve)
            return
        except Exception as e:
            print("In expire_sap_access: What's up with that!", e)
            return
        self.transition_issue("Flowbot Cancelled",
                              "Cancelled due to lack of approval.  "
                              "Please submit a new "
                              "ticket")

    def lock_sap(self):
        try:
            self.change_sap_permissions('BAPI_USER_LOCK')
        except Exception as e:
            print("Not transitioning ticket %s" % self.issue.key)
            raise e