#from builtins import Exception, super
from collections import OrderedDict

from processes.projects.ffp import Ffp
from pyrfc import Connection

class Standard(Ffp):
    '''Handles FFP'''
    def __init__(self, jira, issue, celery_config):
        super(Standard, self).__init__(jira, issue, celery_config)

        itype = issue.fields.issuetype.name
        try:
            self.SAP_SYSID = celery_config['SAP_COMP'][itype]['SAP_SYSID']
            self.SAP_MSHOST = celery_config['SAP_COMP'][itype]['SAP_MSHOST']
            self.SAP_MSSERV = celery_config['SAP_COMP'][itype]['SAP_MSSERV']
            self.SAP_GROUP = celery_config['SAP_COMP'][itype]['SAP_GROUP']
            self.SAP_CLIENT = celery_config['SAP_COMP'][itype]['SAP_CLIENT']
            self.SAP_USER = celery_config['SAP_USER']
            self.SAP_PASSWD = celery_config['SAP_PASSWD']
        except Exception as e:
            print("Problem getting SAP vars for %s" % itype)
            raise Exception(e)
        #print("HI from Standard FFP, %s SAP_SYSID = %s!!!" % (type(
        # self.SAP_SYSID),
        #  self.SAP_SYSID))
        self.synopsis_fields = OrderedDict([
            ('Summary', self.issue.fields.summary),
            ('issuetype', self.issue.fields.issuetype.name),
            ('Reporter', self.issue.fields.reporter.displayName),
            ('Operational Approver',
             self.issue.fields.customfield_19008.displayName),
            ('FireFighter ID', self.issue.fields.customfield_23500)
        ])
