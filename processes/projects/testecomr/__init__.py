from processes.projects import Project
from jenkinsapi.jenkins import Jenkins
import time

class Testecomr(Project):
    def __init__(self, jira, issue, celery_conf):
        super(Testecomr, self).__init__(jira, issue, celery_conf)
        print("HI from Testecomr!!!")
        # Jira assures us that valid brands will be selected
        self.jira_brands = {'J.Crew': 'jcrew',
                            'Factory': 'factory',
                            'Madewell': 'madewell'
                            }
        self.jenkins = Jenkins(self.celery_conf.JENKINS_URL)

    def promote_sidecar(self, env):
        print("Promoting to %s" % env)
        if env == 'uat':
            brands = self.jira_brands.values()
        else:
            try:
                brands = [ self.jira_brands[b.value] for b in
                           self.issue.fields.customfield_13301 ]
            except Exception as e:
                raise ValueError("Can't figure out brands")
        for brand in brands:
            # job name, e.g. promote-sidecar-production-factory
            try:
                job = 'Sidecar-Promote-to-%s-all' % (env.upper())
                params = {
                          'token': self.celery_conf.JENKINS_PROMOTE_TOKEN.get(env.upper()),
                          'block':True
                         }
                print("Running %s with params: %s" % (job, params))
                self.jenkins.build_job(job, params=params)
            except Exception as e:
                self.transition_issue('Flowbot Denied', str(e))
                return
        self.transition_issue('Flowbot Approved')
        # I need to monitor the state of the job to next transition to
        # Flowbot Deployed to QA / Flowbot Deployment Failed

    def monitor_promote(self,env):
        print("Running monitor_promote: %s" % (env))
        if env == 'uat':
            brands = self.jira_brands.values()
            print("Brand in %s for monitor_promote: %s" % (env, brands))
        #elif env == 'production':
        else:
            try:
                brands = [ self.jira_brands[b.value] for b in
                           self.issue.fields.customfield_13301 ]
            except Exception as e:
                raise ValueError("Can't figure out brands")
                return
        print("Brand in monitor_promote: %s" % brands)
        num_still_building = len(brands)
        job = 'Sidecar-Promote-to-%s-all' % (env.upper())
        while num_still_building > 0:
            print("Promotion to %s is on-going.  Sleeping for 10 sec.  %s "
                  "jobs still building" % (env, num_still_building ))
            time.sleep(10)
            num_still_building = 0
            try:
                job = 'Sidecar-Promote-to-%s-all' % (env.upper())
                build = self.jenkins.get_job(job).get_last_build()
                if build.is_running():
                    num_still_building = num_still_building + 1
            except Exception as e:
                self.transition_issue('Flowbot Denied', str(e))
        # At this point, the promote jobs are no longer running, so we need to
        # Check SUCCESS
        try:
            build = self.jenkins.get_job(job).get_last_build()
            if build.get_status() != 'SUCCESS':
                self.transition_issue('Flowbot Denied',
                                      'Please check ansible tower for details')
                return
        except Exception as e:
            self.transition_issue('Flowbot Denied', str(e))
            return
        self.transition_issue('Flowbot Approved')

    def deploy_sidecar(self, env):
        print("Deploying to %s" % env)
        try:
            brands = [ self.jira_brands[b.value] for b in
                       self.issue.fields.customfield_13301 ]
        except Exception as e:
            raise ValueError("Can't figure out brands")
        print("brands is set to %s" % brands)
        for brand in brands:
            # job name, e.g. deploy-sidecar-production-factory
            try:
                job = 'deploy-sidecar-%s-%s' % (env, brand)
                params = {'token': self.celery_conf.JENKINS_DEPLOY_TOKEN.get(env.upper()),
                          'CMS_NUM':self.issue.key,
                          'block': True
                          }
                print("Running %s with params: %s" % (job, params))
                self.jenkins.build_job(job, params=params)
            except Exception as e:
                print("Deployment of %s to %s failed: %s" % (brand, env, e))
                self.transition_issue('Flowbot Denied', str(e))
                return
        self.transition_issue('Flowbot Approved')
        # I need to monitor the state of the job to next transition to
        # Flowbot Deployed to QA / Flowbot Deployment Failed

    def monitor_deploy(self,env):
        try:
            brands = [ self.jira_brands[b.value] for b in
                       self.issue.fields.customfield_13301 ]
        except Exception as e:
            raise ValueError("Can't figure out brands")
        num_still_building = len(brands)
        while num_still_building > 0:
            print("Deployment to %s is on-going.  Sleeping for 1 min.  %s "
                  "jobs still building" % (env, num_still_building ))
            time.sleep(60)
            num_still_building = 0
            for brand in brands:
                try:
                    job = 'deploy-sidecar-%s-%s' % (env, brand)
                    build = self.jenkins.get_job(job).get_last_build()
                    if build.is_running():
                        num_still_building = num_still_building + 1
                except Exception as e:
                    self.transition_issue('Flowbot Denied', str(e))
        # At this point, the deploy jobs are no longer running, so we need to
        # Check SUCCESS
        for brand in brands:
            try:
                job = 'deploy-sidecar-%s-%s' % (env, brand)
                build = self.jenkins.get_job(job).get_last_build()
                if build.get_status() != 'SUCCESS':
                    self.transition_issue('Flowbot Denied',
                                          'Please check ansible tower for details')
                    return
            except Exception as e:
                self.transition_issue('Flowbot Denied', str(e))
                return
        self.transition_issue('Flowbot Approved')

    def aggregate_toggles(self):
        try:
            self.aggregate_child_fields('Toggles')
        except Exception as e:
            self.comment_issue('Could not update toggles.  please contact Eric:\n%s' % str(e))
            return
