# -*- coding: utf-8 -*-

import ftfy
import re
import requests
import sh

class Import(object):
    def __init__(self, key, endpoint, echo=True, dry_run=False):
        self.key = key
        self.records = []
        self.endpoint = endpoint
        self.echo = echo
        self.dry_run = dry_run

    def run(self, data):
        resp = "NOTHING"
        try:
            print("Calling client.call at %s" % self.endpoint)
            print("Data: %s" % data)
            resp = requests.post(self.endpoint, json = data)
        except Exception as e:
            print("Oops in Import.run")
            raise Exception(e)
        return resp

    def reset(self, brand):
        resp = "NOTHING"
        try:
            print("Resetting brand: %s" % brand)
            data = {'brand': brand}
            resp = requests.post(reset_endpoint, json = data)
        except Exception as e:
            print("Oops in Import.reset")
            raise Exception(e)
        return resp

    @staticmethod
    def _fix_relative_links(value):
        return re.sub(
                r'<span class="error">&#91;([^|]*?)\|([^]]*?)&#93;</span>',
                r'<a href="\2">\1</a>', value)

    @staticmethod
    def _drop_paragraph_wrapper(value):
        return re.sub(r'^<p>(.*)</p>$', r'\1', value)

    @staticmethod
    def _process_attributes(attributes):
        for name, value in attributes.items():
            v = ftfy.fix_text(value)
            v = Import._fix_relative_links(v)
            v = v.encode('ascii', 'xmlcharrefreplace').decode()
            v = v.replace('\n', ' ')
            v = Import._drop_paragraph_wrapper(v)
            attributes[name] = v

        header_promo_text = attributes.get('header_promo_text', None)
        header_promo_detail = attributes.get('header_promo_detail', None)

        if header_promo_text:
            attributes[
                'header_promo_text'] = '<span>%s</span>' % header_promo_text

        if header_promo_detail:
            full_len = len(header_promo_detail)
            chunk_len = full_len // 4
            attributes['header_promo_detail'] = header_promo_detail[:chunk_len]
            attributes['header_promo_detail_1'] = header_promo_detail[
                                                  chunk_len:2 * chunk_len]
            attributes['header_promo_detail_2'] = header_promo_detail[
                                                  2 * chunk_len:3 * chunk_len]
            attributes['header_promo_detail_3'] = header_promo_detail[
                                                  3 * chunk_len:]

    def _update_object(self, object_type, object_name, **attributes):
        self._process_attributes(attributes)
        locale = attributes.pop("locale", None)

        if locale:
            for attribute_name, attribute_value in attributes.items():
                self.records.append(
                        "S|MULTILINGUAL_OBJECT_ATTRIBUTE|%s|%s|%s|%s|%s|" % (
                            object_type,
                            object_name,
                            attribute_name,
                            locale,
                            attribute_value))
        else:
            for attribute_name, attribute_value in attributes.items():
                self.records.append(
                        "S|OBJECT_ATTRIBUTE|%s|%s|%s|%s|" % (object_type,
                                                             object_name,
                                                             attribute_name,
                                                             attribute_value))


def _main():
    imp = Import("MWD-999", dry_run=True)
    text = '<p>LAST-MINUTE SHOPPING ISN\'T SO BAD, THANKS TO THIS: <span ' \
           'class="error">&#91;25% OFF YOUR ENTIRE ' \
           'PURCHASE|/gift_guide/GIFTWELLGUIDE.jsp?intcmp=home_globalnav_1' \
           '&#93;</span>.<br/>\nIN STORES &amp; ONLINE. USE CODE ' \
           'YOUGOTTHIS.\n' \
           '<span class="error">&#91;Madewell|/index.jsp&#93;</span></p>'
    detail = '“Free” expedited shipping valid at madewell.com from November ' \
             '17, 2015, 12:01am ET through November 19, 2015, 11:59pm ET on ' \
             'purchases of $200 or more before shipping charges and taxes are ' \
             'added—Offer not valid in Madewell stores or on phone orders. ' \
             'Offer cannot be applied to previous purchases and cannot be ' \
             'redeemed for cash. Free expedited shipping (2-3 business days) ' \
             'only valid on orders shipping to the continental U.S. and U.S. ' \
             'P.O. Boxes. Free shipping will be automatically applied to ' \
             'qualifying orders at checkout. Return requests must be made in ' \
             'accordance with our return policy. Terms of offer are subject to ' \
             'change.'
    imp.update_content("/Content/templates/include/inc_top_nav.jsp",
                       locale="en", header_promo_text=text,
                       header_promo_detail=detail)

    try:
        print(imp.run())
    except sh.ErrorReturnCode as e:
        print(e.stdout.decode('utf-8'))
        print(e.stderr.decode('utf-8'))
