from jenkinsapi.jenkins import Jenkins

class JcJenkins(object):

    def __init__(self, celery_config):
        self.server = Jenkins(celery_config['JENKINS_SERVER']))
        self.auth_tokens = celery_config['AUTH_TOKENS']

    def build_job(self, job_name, **params):
        self.server.build_job(job_name, params)

#curl 'http://nyc-wdevtool-02.jcrew.com:8080/view/Sidecar%20Deploy/job
#        # /deploy-sidecar-production-factory/buildWithParameters?' \
#     'token=AmkoomtOnWiScoltiskatbuHavbofcem' \
#     'CMS_NUM=2173
